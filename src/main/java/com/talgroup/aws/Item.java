package com.talgroup.aws;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "item")
public class Item {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String key;
	
	private String value;
	
	private String getKey() { return this.key; }
	
	private String getValue() { return this.value; }
	
	private void setKey(final String key) { this.key = key; }
	
	private void setValue(final String value) { this.value = value; }

}
