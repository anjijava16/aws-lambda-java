package com.talgroup.aws;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("hello")
@ApplicationScoped
public class HelloController {

	@Inject
	private ItemRepository repository;
	
    @GET
    public String sayHello() {
        return "Hello Java EE from AWS Lambda!";
    }

}
